DROP TABLE IF EXISTS cliente;
CREATE TABLE cliente (
  id_cliente INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL,
  dt_atualizacao TIMESTAMP NOT NULL,
  PRIMARY KEY (id_cliente));