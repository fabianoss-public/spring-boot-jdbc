package br.com.fabianoss.repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import br.com.fabianoss.domain.Cliente;

@Repository
public class ClienteRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // Find all Clientes, thanks Java 8, you can create a custom RowMapper like this
    // :
    public List<Cliente> findAll() {

	List<Cliente> result = jdbcTemplate.query("SELECT id_cliente, username, email, dt_atualizacao FROM cliente",
		(rs, rowNum) -> new Cliente(rs.getLong("id_cliente"), rs.getString("username"), rs.getString("email"),
			rs.getDate("dt_atualizacao")));

	return result;

    }

    // Add new Cliente
    public void addCliente(String username, String email) {

	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
	Date date = new Date();
	dateFormat.format(date);
	
	jdbcTemplate.update("INSERT INTO cliente(username, email, dt_atualizacao) VALUES (?,?,?)", username, email,
		date);

    }

}
