package br.com.fabianoss.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Cliente implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2202975785402674958L;

    private Long idCliente;
    
    private String userName;
    
    private String email;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:SS")
    private Date dataAtualizacao;

    public Cliente(Long idCliente, String userName, String email, Date dataAtualizacao) {
	this.idCliente = idCliente;
	this.userName = userName;
	this.email = email;
	this.dataAtualizacao = dataAtualizacao;
    }

    public Long getIdCliente() {
	return idCliente;
    }

    public void setIdCliente(Long idCliente) {
	this.idCliente = idCliente;
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public Date getDataAtualizacao() {
	return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
	this.dataAtualizacao = dataAtualizacao;
    }

}
